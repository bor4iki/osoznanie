import sqlite3


class SQLighter:

    def __init__(self, database):
        """Подключаемся к БД и сохраняем курсор соединения"""
        self.connection = sqlite3.connect(database, check_same_thread=False)
        self.cursor = self.connection.cursor()

    def get_all_users(self):
        """Подсчитываем количество юзеров"""
        with self.connection:
            request = self.cursor.execute(
                "SELECT COUNT (*)"
                " FROM Users"
            ).fetchall()
            return request

    def check_user(self, user_id):
        with self.connection:
            result = self.cursor.execute(
                "SELECT * FROM Users "
                "WHERE user_id = ?",
                (user_id,)
            ).fetchall()
            return bool(len(result))

    def add_user_db(self, user_id: int, status: bool = True):
        """Добавляем юзера в базу данных"""
        if not self.check_user(user_id):
            with self.connection:
                request = self.cursor.execute(
                    "INSERT INTO Users (user_id, status)"
                    " VALUES(?, ?)",
                    (user_id, status,)
                )
                return request

    def check_user_info(self, user_id: int):
        with self.connection:
            request = self.cursor.execute(
                "SELECT UsersInfo.user_id FROM UsersInfo join Users on UsersInfo.user_id = Users.id "
                "WHERE Users.user_id = ?",
                (user_id,)
            ).fetchall()
            return request

    def add_user_info(self, user_id: int, username: str = 'NULL', first_name: str = 'NULL', last_name: str = 'NULL'):
        if not self.check_user_info(user_id):
            with self.connection:
                request = self.cursor.execute(
                    "INSERT INTO UsersInfo (user_id, username, first_name, last_name)"
                    " VALUES ((SELECT Users.id FROM Users WHERE Users.user_id = ?), ?, ?, ?)",
                    (user_id, username, first_name, last_name,)
                )
                return request

    def add_new_hbd(self, user_id: int, name: str, day: int, month: int, year: int = 0):
        with self.connection:
            request = self.cursor.execute(
                "INSERT INTO HBD (user_id, name, day, month, year)"
                " VALUES ((SELECT Users.id FROM Users WHERE Users.user_id = ?), ?, ?, ?, ?)",
                (user_id, name, day, month, year,)
            )
            return request

    def check_status(self, user_id: int):
        with self.connection:
            request = self.cursor.execute(
                "select status from Users where user_id = ?",
                (user_id,)
            ).fetchall()
            status, = request[0]
            return bool(status)

    def get_hbd(self, user_id: int):
        with self.connection:
            request = self.cursor.execute(
                "select HBD.id, name, day, month, year "
                "from HBD join Users on HBD.user_id = Users.id "
                "where Users.user_id = ?"
                "order by month, day",
                (user_id,)
            ).fetchall()
            return request if self.check_status(user_id) else False

    def get_hbd_order(self, user_id: int):
        with self.connection:
            request = self.cursor.execute(
                "select name from HBD join Users on HBD.user_id = Users.id "
                "where Users.user_id = ? "
                "order by month, day",
                (user_id,)
            ).fetchall()
            return request

    def update_status(self, user_id: int, status: bool):
        with self.connection:
            request = self.cursor.execute(
                "update Users set status = ? where user_id = ?",
                (status, user_id,)
            )
            return request

    def get_hbd_from_id(self, id_):
        with self.connection:
            request = self.cursor.execute(
                "select name, day, month, year from HBD "
                "where id = ?",
                (id_, )
            ).fetchall()
            return request

    def update_name(self, id_: int, name: str):
        with self.connection:
            request = self.cursor.execute(
                "update HBD set name = ? where id = ?",
                (name, id_, )
            )
            return request

    def update_date(self, id_: int, day: int, month: int, year: int = 0):
        with self.connection:
            request = self.cursor.execute(
                "update HBD set day = ?, month = ?, year = ? where id = ?",
                (day, month, year, id_)
            )
            return request

    def delete_line(self, id_: int):
        with self.connection:
            request = self.connection.execute(
                "delete from HBD where id = ?",
                (id_, )
            )
            return request

    def get_userid(self):
        """Вывод всех юзер айди"""
        with self.connection:
            request = self.cursor.execute(
                "SELECT user_id "
                "FROM Users"
            ).fetchall()
            # print(request)
            return request

    def close(self):
        """Закрываем соединение с БД"""
        self.connection.close()


if __name__ == '__main__':
    db = SQLighter('..\\..\\bot_anastasia.db')
    users = db.get_userid()
    # print(users)
    # for user, in users:
    #     print(user)
    # db.add_user_db(5555, 'test_1', True)
    # db.get_all_users()
    # db.add_new_hbd(5555, 'Др папы', 2, 9, 1963)
    # bd.add_user_info(2222, 'ttt', 'tttt', 'ttttt')
    # print(db.check_user(2222))
    # db.update_status(1111, True)
    # db.delete_line(53)
    # print(db.get_hbd_from_id(53))
    # print(db.get_hbd_from_id(53))
    # print(db.get_hbd(870769152))
