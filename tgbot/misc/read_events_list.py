FILE_NAME = 'events_list.txt'


def read_events(file_name=FILE_NAME):
    list_ = []
    with open(file_name, 'r', encoding='utf-8') as file:
        for i, f in enumerate(file):
            list_.append(f'{i+1} {f}')
    return list_
