from aiogram.dispatcher.filters.state import State, StatesGroup


class MessForAllUsers(StatesGroup):
    text_message = State()
    bool_state = State()


class ChoiseEvent(StatesGroup):
    event = State()
    ticket = State()


class SingUp(StatesGroup):
    message_text = State()
