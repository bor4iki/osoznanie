from aiogram import types

from tgbot.models.db_main import SQLighter


async def add_user_to_db(db: SQLighter, message: types.Message):
    user_id = message.from_user.id
    username = message.from_user.username
    first_name = message.from_user.first_name
    last_name = message.from_user.last_name
    db.add_user_db(user_id=user_id)
    db.add_user_info(user_id=user_id, username=username, first_name=first_name, last_name=last_name)
    # logging.info(f'└---->{message.from_user.full_name} add to db')
