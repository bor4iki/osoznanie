from aiogram.types.inline_keyboard import InlineKeyboardButton, InlineKeyboardMarkup

bool_admin_message_key = InlineKeyboardMarkup(
    inline_keyboard=
    [
        [
            InlineKeyboardButton(text="Да", callback_data="yes"),
            InlineKeyboardButton(text="Неть", callback_data="no")
        ],
    ]
)

contacts_inline_key = InlineKeyboardMarkup(
    inline_keyboard=
    [
        [
            InlineKeyboardButton(text="Ссылка на тг канал",
                                 url='https://t.me/centre_osoznanie_news',
                                 callback_data='tg_channel')
        ],
        # [
        #     InlineKeyboardButton(text='Ссылка на сайт',
        #                          url='https://www.google.ru/',
        #                          callback_data='website')
        # ]
    ]
)

about_inline_key = InlineKeyboardMarkup(
    inline_keyboard=
    [
        [
            InlineKeyboardButton(text='Наши партнеры', url='https://www.google.ru/', callback_data='our_partners')
        ],
        [
            InlineKeyboardButton(text='Наша команда', url='https://www.google.ru/', callback_data='our_team')
        ]
    ]
)


def inline_list_events(data: list):
    markup = InlineKeyboardMarkup()
    for i in data:
        markup.add(
            InlineKeyboardButton(i[0], callback_data=f'event:{i[0]}')
        )
    markup.row_width = 3
    return markup
