from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

admin_key = ReplyKeyboardMarkup(
    keyboard=
    [
        [
            KeyboardButton(text='Написать сообщение всем пользователям'),
        ],
        [
            KeyboardButton(text='Количество юзеров в боте'),
        ],
        [
            KeyboardButton(text='Ссылка на канал с логами')
        ],
    ],
    resize_keyboard=True
)

user_key = ReplyKeyboardMarkup(
    keyboard=
    [
        [
            # KeyboardButton(text='Мероприятия'),
            KeyboardButton(text='Запись на бесплатную консультацию'),
        ],
        [
            KeyboardButton(text='Хочу подарок 🎁'),
            KeyboardButton(text='Услуги'),
            KeyboardButton(text='Вопрос менеджеру'),
        ],
        [
            KeyboardButton(text='О компании'),
        ]
    ],
    resize_keyboard=True
)

services_key = ReplyKeyboardMarkup(
    keyboard=
    [
        [
            KeyboardButton(text='Запись на бесплатную консультацию'),
        ],
        [
            KeyboardButton(text='Вопрос менеджеру'),
            # KeyboardButton(text='Узнать подробнее'),
        ],
        [
            KeyboardButton(text='На главную')
        ]
    ],
    resize_keyboard=True
)

events_key = ReplyKeyboardMarkup(
    keyboard=
    [
        [
            KeyboardButton(text='Календрарь мероприятий')
        ],
        [
            KeyboardButton(text='На главную')
        ]
    ],
    resize_keyboard=True
)

choice_events_key = ReplyKeyboardMarkup(
    keyboard=
    [
        [
            KeyboardButton(text='Забронировать место бесплатно')
        ],
        [
            KeyboardButton(text='Купить VIP-билет')
        ],
        [
            KeyboardButton(text='На главную')
        ]
    ],
    resize_keyboard=True
)

about_key = ReplyKeyboardMarkup(
    keyboard=
    [
        [
            KeyboardButton(text='Наши резиденты')
        ],
        [
            KeyboardButton(text='На главную')
        ]
    ],
    resize_keyboard=True
)