import logging

from aiogram import Dispatcher

from environs import Env

from tgbot.config import channel_to_post
from tgbot.keyboards.reply import admin_key

env = Env()
env.read_env('.env')

ADMINS = list(map(int, env.list("ADMINS")))


async def on_startup_notify(dp: Dispatcher):
    await dp.bot.send_message(channel_to_post, "Старт бота!")
    for admin in ADMINS:
        try:
            await dp.bot.send_message(admin, "Бот Запущен", reply_markup=admin_key)

        except Exception as err:
            logging.exception(err)
