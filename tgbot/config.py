from dataclasses import dataclass

from environs import Env

from aiogram import Bot

from tgbot.models.db_main import SQLighter


@dataclass
class DbConfig:
    host: str
    password: str
    user: str
    database: str


@dataclass
class TgBot:
    token: str
    admin_ids: list[int]
    use_redis: bool


@dataclass
class Miscellaneous:
    other_params: str = None


@dataclass
class Config:
    tg_bot: TgBot
    # db: DbConfig
    misc: Miscellaneous


env = Env()


def load_config(path: str = None):
    env.read_env(path)

    return Config(
        tg_bot=TgBot(
            token=env.str("BOT_TOKEN"),
            admin_ids=list(map(int, env.list("ADMINS"))),
            use_redis=env.bool("USE_REDIS"),
        ),
        # db=DbConfig(
        #     host=env.str('DB_HOST'),
        #     password=env.str('DB_PASS'),
        #     user=env.str('DB_USER'),
        #     database=env.str('DB_NAME')
        # ),
        misc=Miscellaneous()
    )


config = load_config(".env")

env.read_env('.env')
bot = Bot(token=config.tg_bot.token, parse_mode='HTML')
db = SQLighter('bot_anastasia.db')

channel_to_post = env.str("CHANNEL_TO_POST")
channel_events = env.str("CHANNEL_EVENTS")
