import asyncio

from aiogram import Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from aiogram.types import Message, ReplyKeyboardRemove, CallbackQuery, ContentTypes

from tgbot.config import db, bot
from tgbot.keyboards.inline import bool_admin_message_key
from tgbot.keyboards.reply import admin_key
from tgbot.misc.add_new_user import add_user_to_db
from tgbot.misc.states import MessForAllUsers


async def admin_start(message: Message):
    print(f'{message.from_user.id} start bot')
    await add_user_to_db(db, message)
    await message.reply("Hello, admin!", reply_markup=admin_key)


async def number_of_users(message: Message):
    # logging.info(f'{message.from_user.id}/{message.from_user.full_name} use command count_of_users')
    print(f'{message.from_user.id} watch count of users')
    count_users, = db.get_all_users()
    await message.answer(f"Количество юзеров в боте : <b>{count_users[0]}</b>")


async def admin_message_for_all_users(message: Message):
    # logging.info(f'{message.from_user.id}/{message.from_user.full_name} start write message for all users')
    print(f'{message.from_user.id} start send message for all')
    await MessForAllUsers.text_message.set()
    await message.answer(f"Напиши мне сообщение ниже, которое хочешь отправить всем пользователям",
                         reply_markup=ReplyKeyboardRemove())


async def message_text_for_all(message: Message, state: FSMContext):
    # logging.info(f'{message.from_user.id}/{message.from_user.full_name} in {await state.get_state()}'
    #              f' write some message')
    print(f'{message.from_user.id} start send message for all')
    async with state.proxy() as data:
        data['text_message'] = message
    # TODO при отправке альбома (больше одной фотографии) отправится только первая. 30.08 решение не найдено
    await message.answer("Вот так будет выглядеть твое сообщение")
    await asyncio.sleep(1)
    if message.photo:
        print(f'{message.from_user.id} send photo')
        file_id = message.photo[0].file_id
        caption = message.caption
        await message.answer_photo(file_id, caption=caption if caption else '')
    elif message.text:
        print(f'{message.from_user.id} send text')
        await message.answer(message.text)
    elif message.document:
        print(f'{message.from_user.id} send document')
        document_id = message.document.file_id
        caption = message.caption
        await message.answer_document(document_id, caption=caption if caption else '')
    elif message.video:
        print(f'{message.from_user.id} send video')
        video_id = message.video.file_id
        caption = message.caption
        await message.answer_video(video_id, caption=caption if caption else '')
    # TODO возможно потребуется дописать для стикеров
    await asyncio.sleep(1)
    await message.answer("Ты все правильно написал?", reply_markup=bool_admin_message_key)
    await MessForAllUsers.bool_state.set()


async def confirm_message(call: CallbackQuery, state: FSMContext):
    print(f'{call.message.chat.id} say Yes to send message')
    await call.message.edit_reply_markup()
    users = db.get_userid()
    data = await state.get_data()
    text_message: Message = data['text_message']
    for user, in users:
        if text_message.text:
            # print(f'{call.message.from_user.id} bot send text message')
            await bot.send_message(user, text_message.text)
            await asyncio.sleep(0.3)
        elif text_message.photo:
            # print(f'{call.message.from_user.id} bot send photo message')
            file_id = text_message.photo[0].file_id
            caption = text_message.caption
            await bot.send_photo(user, file_id, caption=caption if caption else '')
            await asyncio.sleep(0.3)
        elif text_message.document:
            # print(f'{call.message.from_user.id} bot send document message')
            document_id = text_message.document.file_id
            caption = text_message.caption
            await bot.send_document(user, document_id, caption=caption if caption else '')
            await asyncio.sleep(0.3)
        elif text_message.video:
            # print(f'{call.message.from_user.id} bot send video message')
            video_id = text_message.video.file_id
            caption = text_message.caption
            await bot.send_document(user, video_id, caption=caption if caption else '')
            await asyncio.sleep(0.3)
    await call.message.answer('Сообщение отправлено!', reply_markup=admin_key)
    print('message send')
    await state.finish()


async def cancel_send_message_for_all(call: CallbackQuery, state: FSMContext):
    print(f'{call.message.chat.id} say No to send message')
    await call.message.edit_reply_markup()
    await call.message.answer('Окей, ты отменил задание отправлять сообшение всем.\n\n'
                              'Нажми на /start и через секунду я отпралю тебе начальноее сообщение')
    await asyncio.sleep(1)
    await state.finish()


async def channel(message: Message):
    print(f'{message.from_user.id} look at link of channel')
    await message.answer('https://t.me/chanel_anastasia')


def register_admin(dp: Dispatcher):
    dp.register_message_handler(admin_start, commands=["start"], state="*", is_admin=True)
    dp.register_message_handler(number_of_users, Text(equals=['Количество юзеров в боте']), state="*", is_admin=True)
    dp.register_message_handler(channel, Text(equals=['Ссылка на канал с логами']))
    dp.register_message_handler(
        admin_message_for_all_users,
        Text(equals=['Написать сообщение всем пользователям']), state="*", is_admin=True
    )
    dp.register_message_handler(
        message_text_for_all,
        state=MessForAllUsers.text_message,
        content_types=ContentTypes.ANY,
        is_admin=True
    )
    dp.register_callback_query_handler(
        confirm_message,
        state=MessForAllUsers.bool_state,
        text_contains="yes",
        is_admin=True
    )
    dp.register_callback_query_handler(
        cancel_send_message_for_all,
        state=MessForAllUsers.bool_state,
        text_contains="no",
        is_admin=True
    )
