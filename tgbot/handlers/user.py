import asyncio

from aiogram import Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from aiogram.types import Message, CallbackQuery, ReplyKeyboardRemove

from tgbot.config import db, bot, channel_to_post, channel_events
from tgbot.keyboards.inline import contacts_inline_key, about_inline_key, inline_list_events
from tgbot.keyboards.reply import user_key, about_key, services_key, events_key, choice_events_key
from tgbot.misc.add_new_user import add_user_to_db


# <a href="tg://user?id=123456789">inline mention of a user</a>
from tgbot.misc.link_photos import link_second_start_message, link_bot_about_message, link_services_message, \
    link_contacts_message
from tgbot.misc.read_events_list import read_events
from tgbot.misc.states import ChoiseEvent, SingUp


def user_info(message: Message):
    mes = f'<i>Информация по юзеру</i>\n\n' \
          f'<a href="tg://user?id={message.from_user.id}">' \
          f'{message.from_user.first_name if message.from_user.first_name else "Юзер"}</a>\n' \
          f'<a>@{message.from_user.username if message.from_user.username else ""}</a>\n' \
          f'<code>{message.from_user.last_name if message.from_user.last_name else ""}</code>'
    return mes


async def on_start_page(message: Message):
    await user_start(message)


async def user_start(message: Message):
    await add_user_to_db(db, message)
    first_msg = """Начинаем знакомство🙃 
У меня интересный функционал, нажимай кнопку, которая тебя заинтересовала, а пока я тебе расскажу о нашем мире 🌐. """
    second_msg = """“оСознание” - это программа курсов, методик и практик, которая дает возможность любому человеку изменить свою жизнь во всех сферах🗣👨‍👩‍👧🗺💵. 
Наш центр позволяет не только проходить различные курсы и практики, но и научиться жить в осознанности с собой и окружающим миром🧘‍♀️.  
Главной целью является помощь людям, которые готовы начать новый жизненный путь и меняться вместе с нами💪."""
    await message.answer(first_msg, reply_markup=user_key)
    # await message.answer(second_msg,
    #                      reply_markup=contacts_inline_key)
    await message.answer_photo(photo=link_second_start_message, caption=second_msg, reply_markup=contacts_inline_key)
    mes = user_info(message)
    mess = 'Юзер стратанул бота\n\n' + mes
    await bot.send_message(channel_to_post, mess)


async def sing_up(message: Message):
    msg = """Напиши контактную информацию для связи (ник телеграмма, номер телефона и тд.) на выбор, опиши свой запрос для нашего специалиста, я им передам и наша команда с тобой свяжется, чтобы уточнить время и дату созвона ☎️ 📅.
 
Не бойся меняться, я в тебя верю, ты справишься🌅🖤"""
    # await message.answer(msg, reply_markup=ReplyKeyboardRemove())
    await message.answer_photo(photo=link_contacts_message, caption=msg, reply_markup=ReplyKeyboardRemove())
    await SingUp.message_text.set()
    mes = f'Юзер перешел на вкладку запись на консультацю\n\n{user_info(message)}'
    await bot.send_message(channel_to_post, mes)


async def sing_up_message_text(message: Message, state: FSMContext):
    async with state.proxy() as data:
        data['message_text'] = message.text

    await message.answer('<b>Передал информацию команде!</b>')
    msg = f'<i>Текст пользователя</i>:\n {message.text} \n\n {user_info(message)}'
    await bot.send_message(channel_events, text=msg)
    await state.finish()
    await asyncio.sleep(2)
    await on_start_page(message)


async def bot_about(message: Message):
    first_msg = """У нас большая команда экспертов и профи в своём деле, которые будут рядом с тобой на протяжении всего пути твоей трансформации💫
 
Работаем с различными запросами в разных сферах (бизнес, семья, отношения, депрессия, страхи, мышление). Создаём большую партнёрскую сеть🤝, благодаря чему, открываем ещё больше возможностей для тебя.  
А ещё мы любим творить добро и занимаемся благотворительностью)

 При центре есть клуб “Сознание”, в нём ты сможешь найти единомышленников, партнёров, друзей, просто с пользой провести время и  прокачать свои навыки: 
-  научиться самопрезентации 
-  коммуникационные и ораторские навыки 
- управленческие 
- Уверенность и любовь к себе 
- бизнес-видение и многие другие. 
 
Я могу очень долго рассказывать о нас, но лучше тебе один раз увидеть и попробовать, чем читать этот длинный текст😉🙈"""
    second_msg = """"Мы никогда не сможем добиться осознанности от внешнего мира, пока не добьёмся этого в себе"
P.S. Рудик🥸
 
😉🙈💫"""
    # await message.answer(first_msg, reply_markup=about_key)
    await message.answer_photo(photo=link_bot_about_message, caption=first_msg, reply_markup=about_key)
    await message.answer(second_msg, reply_markup=about_inline_key)
    mes = f'Юзер перешел на вкладку О компании\n\n{user_info(message)}'
    await bot.send_message(channel_to_post, mes)


async def bot_help(message: Message):
    text = ("Список команд: ",
            "/start - Начать диалог",
            "/help - Получить справку",
            "/about - О компании")
    await message.answer("\n".join(text))
    mes = f'Юзер перешел на вкладку /help\n\n{user_info(message)}'
    await bot.send_message(channel_to_post, mes)


async def services(message: Message):
    msg = """
- Йога 🧘‍♂️🤸 
Духовные и физические практики для достижения познания тела и   осознанности.
 
- Медитации (различные виды)🧘🏻‍♀️💫🧿 
Духовная практика, направленная на самопознание , обретение гармонии и спокойствия с миром.
 
- Дыхательные практики 🌬 
Комплекс упражнений, направленный на улучшение тонуса и работы организма.
 
- Индивидуальная программа ✍️👩‍💻 
Программа работы зависит от первичной диагностики и составляется квалифицированными специалистами, исходя из выявленного запроса и области работы.
 
- Курсы (онлайн и офлайн) и гайды👨‍⚕👩‍⚕🧠 
Индивидуальные методики и подходы, разработанные специалистами на основе доказательной медицины и не только, многолетнего опыта и практики в экспертной сфере специалиста. 
 
Ты уже понял/поняла, что у нас широкий спектр услуг (это не весь список)😉, приходи к нам в гости и наконец-то познакомимся☕️👥 
 """
    # await message.answer(msg, reply_markup=services_key)
    await message.answer_photo(photo=link_services_message, caption=msg, reply_markup=services_key)
    mes = f'Юзер перешел на вкладку услуги\n\n{user_info(message)}'
    await bot.send_message(channel_to_post, mes)


async def events(message: Message):
    await message.answer('Текст про мероприятия компании', reply_markup=events_key)
    mes = f'Юзер перешел на вкладку мероприятия\n\n{user_info(message)}'
    await bot.send_message(channel_to_post, mes)


async def choice_events(message: Message):
    data = read_events()
    msg_text = ''
    for s in data:
        msg_text += s
    await message.answer(f'<b>Календарь мероприятий</b>\n\n {msg_text}',
                         reply_markup=inline_list_events(data=data))
    await ChoiseEvent.event.set()
    mes = f'Юзер перешел на вкладку Календарь мероприятий\n\n{user_info(message)}'
    await bot.send_message(channel_to_post, mes)


async def selected_event_call(call: CallbackQuery, state: FSMContext):
    await call.message.edit_reply_markup()
    number_event = int(call.data.split(':')[1]) - 1
    msg = ''
    data = read_events()
    for num, line in enumerate(data):
        if number_event == num:
            msg += line[2:]
    async with state.proxy() as data:
        data['event'] = msg
    await ChoiseEvent.ticket.set()
    await call.message.answer(f'Пользователь выбрал мероприятие под названием\n\n{msg}', reply_markup=choice_events_key)


async def choose_ticket(message: Message, state: FSMContext):
    async with state.proxy() as data:
        if 'VIP' in message.text:
            data['ticket'] = 'VIP'
        else:
            data['ticket'] = 'free'
    ticket = data['ticket']
    event = data['event']
    if 'Забронировать место бесплатно' in message.text:
        msg_for_manager = f'<b>Запись на мероприятие</b>\n\n' \
                          f'Event : {event}\nTicket : {ticket}\n\n' \
                          f'{user_info(message)}\n\n'
        await message.answer(f'Вы выбрали <i>{event}</i> и забронировали <i>бесплатное</i> место\n\n'
                             f'Скоро с вами свяжется наш менеджер'
                             , reply_markup=ReplyKeyboardRemove())
        await state.finish()
        await asyncio.sleep(2)
        await user_start(message)
        await bot.send_message(channel_events, text=msg_for_manager)
    elif 'Купить VIP-билет' in message.text:
        msg_for_manager = f'<b>Запись на мероприятие</b>\n\n' \
                          f'Event : {event}\nTicket : {ticket}\n\n' \
                          f'{user_info(message)}\n\n'
        await message.answer(f'Вы выбрали : <i>{event}</i> и забронировали <b>VIP</b> место\n\n'
                             f'Скоро с вами свяжется наш менеджер'
                             , reply_markup=ReplyKeyboardRemove())
        await state.finish()
        await asyncio.sleep(2)
        await user_start(message)
        await bot.send_message(channel_events, text=msg_for_manager)
    else:
        print('Stop state')
        await state.finish()


async def want_gift(message: Message):
    msg = """Любишь подарочки? 🎁 
Я тоже☺️) А ещё больше я люблю их дарить, поэтому лови от меня подарок! 🎟

Промокод: START2022 на скидку в 10% на любую услугу центра до конца сентября, только не говори, что это я тебе его дал, тсссс🙈❤️

Вступив в нашу телеграмм-группу, ты сможешь получать подарки чаще и быть в курсе всех событий самым первым!😎 
Залетай туда, тебя там все заждались!🙎‍♀️🙎‍♂️🙎👋"""
    await message.answer(msg, reply_markup=contacts_inline_key)
    mes = f'Юзер перешел на вкладку Подарок\n\n{user_info(message)}'
    await bot.send_message(channel_to_post, mes)


async def ask_manager(message: Message):
    msg = """Напиши контактную информацию для связи (ник телеграмма, номер телефона и тд.) на выбор, опиши свой вопрос и наша команда с тобой свяжется, чтобы помочь ✍️ 📱 📧 
Пока ожидаешь ответа, жми кнопку “Хочу подарок🎁” и регистрируйся на мероприятия. 
На связи😉"""
    # await message.answer(msg, reply_markup=ReplyKeyboardRemove())
    await message.answer_photo(photo=link_contacts_message, caption=msg, reply_markup=ReplyKeyboardRemove())
    await SingUp.message_text.set()
    mes = f'Юзер перешел на вкладку Вопрос менеджеру\n\n{user_info(message)}'
    await bot.send_message(channel_to_post, mes)


async def our_residents(message: Message):
    await message.answer('Список резидентов')
    mes = f'Юзер вывел список резидентов\n\n{user_info(message)}'
    await bot.send_message(channel_to_post, mes)


def register_user(dp: Dispatcher):
    dp.register_message_handler(user_start, commands=["start"], state="*")
    dp.register_message_handler(bot_about, Text(equals=['/about', 'О компании']), state="*")
    dp.register_message_handler(bot_help, commands=["help"], state="*")
    dp.register_message_handler(sing_up, Text(equals=['Запись на бесплатную консультацию']), state="*")
    dp.register_message_handler(services, Text(equals=['Услуги']), state="*")
    dp.register_message_handler(events, Text(equals=['Мероприятия']), state="*")
    dp.register_message_handler(choice_events, Text(equals=['Календрарь мероприятий']), state="*")
    dp.register_message_handler(on_start_page, Text(equals=['На главную']), state="*")
    dp.register_message_handler(want_gift, Text(equals=['Хочу подарок 🎁']), state="*")
    dp.register_message_handler(ask_manager, Text(equals=['Вопрос менеджеру']), state="*")
    dp.register_message_handler(our_residents, Text(equals=['Наши резиденты']), state="*")
    dp.register_callback_query_handler(selected_event_call,
                                       Text(startswith='event:'), state=ChoiseEvent.event)
    dp.register_message_handler(choose_ticket, state=ChoiseEvent.ticket)
    dp.register_message_handler(sing_up_message_text, state=SingUp.message_text)
