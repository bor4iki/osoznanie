from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram.utils.markdown import hcode

from tgbot.config import bot, channel_to_post


async def bot_echo(message: types.Message):
    text = """Интересное сообщение, но очень не понятное, а если поподробнее?)🙈🙉🥸 
 
Если ты хочешь получить от меня ответ на свой вопрос, мне нужно, чтоб ты четко следовал инструкции 📝"""
    await message.answer(text)
    await bot.send_message(
        channel_to_post,
        f'<a href="tg://user?id={message.from_user.id}">{message.from_user.first_name}</a>'
        f' просто отправил сообщение с текстом:\n'
        f'{message.text}'
    )


async def bot_echo_all(message: types.Message, state: FSMContext):
    state_name = await state.get_state()
    text = [
        f'Эхо в состоянии {hcode(state_name)}',
        'Содержание сообщения:',
        hcode(message.text)
    ]
    await message.answer('\n'.join(text))


def register_echo(dp: Dispatcher):
    dp.register_message_handler(bot_echo)
    dp.register_message_handler(bot_echo_all, state="*", content_types=types.ContentTypes.ANY)
