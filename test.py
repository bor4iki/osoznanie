from environs import Env

env = Env()
env.read_env('.env')

ADMINS = list(map(int, env.list("ADMINS")))

for ad in ADMINS:
    print(ad)
    print(type(ad))

print(ADMINS)
print(type(ADMINS))
